Pathway_name	id_pathways	num_total_paths	num_significant_paths	percent_significant_paths	num_up_paths	percent_up_paths	num_down_paths	percent_down_paths
NF-kappa B signaling pathway	hsa04064	31	27	87.1	27	87.1	0	0
Toll-like receptor signaling pathway	hsa04620	18	13	72.22	13	72.22	0	0
PI3K-Akt signaling pathway	hsa04151	28	19	67.86	11	39.29	8	28.57
Apoptosis	hsa04210	33	19	57.58	13	39.39	6	18.18
RIG-I-like receptor signaling pathway	hsa04622	7	4	57.14	4	57.14	0	0
TNF signaling pathway	hsa04668	14	8	57.14	8	57.14	0	0
Neurotrophin signaling pathway	hsa04722	12	5	41.67	4	33.33	1	8.33
Chemokine signaling pathway	hsa04062	13	5	38.46	1	7.69	4	30.77
Ras signaling pathway	hsa04014	32	12	37.5	7	21.88	5	15.62
Wnt signaling pathway	hsa04310	12	4	33.33	3	25	1	8.33
Focal adhesion	hsa04510	12	4	33.33	3	25	1	8.33
T cell receptor signaling pathway	hsa04660	10	3	30	2	20	1	10
MAPK signaling pathway	hsa04010	28	8	28.57	7	25	1	3.57
cAMP signaling pathway	hsa04024	35	10	28.57	7	20	3	8.57
Hedgehog signaling pathway	hsa04340	15	4	26.67	0	0	4	26.67
Adrenergic signaling in cardiomyocytes	hsa04261	17	4	23.53	4	23.53	0	0
Adipocytokine signaling pathway	hsa04920	15	3	20	2	13.33	1	6.67
Adherens junction	hsa04520	16	3	18.75	1	6.25	2	12.5
HIF-1 signaling pathway	hsa04066	31	3	9.68	1	3.23	2	6.45
Insulin signaling pathway	hsa04910	15	1	6.67	0	0	1	6.67
Sphingolipid signaling pathway	hsa04071	11	0	0	0	0	0	0
Cell cycle	hsa04110	7	0	0	0	0	0	0
mTOR signaling pathway	hsa04150	3	0	0	0	0	0	0
TGF-beta signaling pathway	hsa04350	6	0	0	0	0	0	0
